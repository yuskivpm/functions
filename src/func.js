/**
 * @typedef Comment
 * @type { object }
 * @property { number } id
 * @property { String } comment
 * @property { String } title
 * @property { String } author
 * @property { Comment[] } comments
 */

/**
 * @typedef Post
 * @type { object }
 * @property { String } id
 * @property { String } post
 * @property { String } title
 * @property { String } author
 * @property { Comment[] } comments
 */

/**
 * task 3
 * @param { Post[] } listOfPosts array with posts
 * @param { String } authorName
 * @return { String } `Post:${number},comments:${number}`
 */
const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const { postCount: resultPostCount, commentCount: resultCommentCount } = listOfPosts.reduce(
    ({ postCount, commentCount }, { author, comments }) => {
      if (author === authorName) {
        postCount++;
      }
      if (comments) {
        comments.forEach(({ author: commentAuthors }) => {
          if (commentAuthors === authorName) {
            commentCount++;
          }
        });
      }
      return { postCount, commentCount };
    }
    , { postCount: 0, commentCount: 0 });

  return `Post:${resultPostCount},comments:${resultCommentCount}`;
};

/**
 * task 1
 * @param { number[] } persons queue Array with bills
 * @param { number } ticketCost default ticket value
 * @param { number[] } availableBills an array of banknotes with a list of denominations
 * @return { String } Yes / No
 */
const TICKET_COST = 25;
const AVAILABLE_BILLS = [25, 50, 100];

const tickets = (people, ticketCost = TICKET_COST, availableBills = AVAILABLE_BILLS) => {
  const cashRegister = availableBills.map(_ => 0);
  const getBillIndex = bill => availableBills.findIndex(value => value === bill);
  return people.every(bill => {
    let currentCashedBillIndex = getBillIndex(bill);
    if (currentCashedBillIndex < 0) {
      // throw new Error('Counterfeit banknote found');
      return false;
    }
    cashRegister[currentCashedBillIndex]++;
    while (bill > ticketCost && currentCashedBillIndex >= 0) {
      if (cashRegister[currentCashedBillIndex] && availableBills[currentCashedBillIndex] < bill) {
        cashRegister[currentCashedBillIndex]--;
        bill -= availableBills[currentCashedBillIndex];
      } else {
        currentCashedBillIndex--;
      }
    }
    return currentCashedBillIndex >= 0;
  })
    ? 'YES'
    : 'NO';
};

/**
 * task 2
 * @param { string } str1 huge number as string
 * @param { string } str1 huge number as string
 * @return { String } sum
 */
const TEN_BASE = 10;
const isValidHugeNumberString = str => typeof str === 'string' && str.match(/[^\d]/) === null;
const getSum = (str1, str2) => {
  if (!isValidHugeNumberString(str1) || !isValidHugeNumberString(str2)) {
    return false;
  }
  const number1 = str1.split('');
  const number2 = str2.split('');
  const result = [];
  let next = 0;
  while (next || number1.length || number2.length) {
    const curDigit = ~~number1.pop() + ~~number2.pop() + next;
    result.unshift(curDigit % TEN_BASE);
    next = Math.floor(curDigit / TEN_BASE);
  }
  return result.join('');
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
